const express = require('express');
const bodyParser = require('body-parser');
const spawn = require('child_process').spawn
const schedule = require('node-schedule');
const fs = require('fs');
const path = require('path')

const app = express();

const port = 3000;

let almostCron = {
  start: () => {
    let raw = fs.readFileSync('schedule.json')
    let schedules = JSON.parse(raw)
    let morning = schedules.morning.split(':')
    let evening = schedules.evening.split(':')
    almostCron.morning = schedule.scheduleJob(`${morning[1]} ${morning[0]} * * *`, function(){
      spawn('python', ['Control2.py'])
    })
    almostCron.evening = schedule.scheduleJob(`${evening[1]} ${evening[0]} * * *`, function(){
      spawn('python', ['Control2.py'])
    })
  },
  morning:'',
  evening:''
}

almostCron.start();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static('client'))

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


app.get('/', (req, res, next) => {
  res.sendFile(path.join(__dirname + '/client/index.html'));
})

app.get('/motor/run', (req, res, next) => {
  spawn('python', ['Control2.py'])
  res.send('motor is running')
})

app.route('/motor/schedule')
  .get((req, res, next) => {
    let raw = fs.readFileSync('schedule.json')
    let schedules = JSON.parse(raw)
    // res.send(`Morning: ${schedules.morning} Evening: ${schedules.evening}`)
    res.json(schedules)
  })
  .post((req, res, next) => {
    let update = JSON.stringify(req.body)
    fs.writeFileSync('schedule.json', update)
    almostCron.morning.cancel();
    almostCron.evening.cancel();
    almostCron.start();
    res.json(req.body)
  })

app.get('/word/:word/:change', (req, res, next) => {
  //res.send(req.params)
  let returnObject;
  if(req.params.change == 'upper'){
    returnObject = {word: req.params.word.toUpperCase()}
  }else if(req.params.change == 'lower'){
    returnObject = {word: req.params.word.toLowerCase()}
  }
  res.json(returnObject)
})

app.post('/posts', (req, res, next) => {

  console.log(req.body)
  
  res.json({theReturn: 'nice'})
})

app.listen(port, () => {
  console.log(`Server started on port: ${port}`)
});