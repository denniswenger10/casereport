const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path')
const historyAPI = require('connect-history-api-fallback');

const app = express();

const port = 3000;

app.use(express.static('techbar/dist'));
app.use(historyAPI());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

const getJsonFile = (file = './data/cases.json') => {
  let raw = fs.readFileSync(file)
  return JSON.parse(raw)
}

const setJsonFile = (obj, file = './data/cases.json') => {
  let update = JSON.stringify(obj)
  fs.writeFileSync(file, update)
}

app.get('/', (req, res, next) => {
  res.sendFile(path.join(__dirname + './index.html'));
})

app.use('/techbar', express.static(__dirname));

app.get('/test', (req, res, next) => {
  res.send('Test worked!')
})

app.route('/cases')
  .get((req, res, next) => {
    res.send(getJsonFile())
  })
  .post((req, res, next) => {
    //let update = JSON.stringify(req.body)

    let db = getJsonFile()
    db.push(req.body)
    setJsonFile(db)

    res.json(db)
  })


app.get('/word/:word/:change', (req, res, next) => {
  //res.send(req.params)
  let returnObject;
  if(req.params.change == 'upper'){
    returnObject = {word: req.params.word.toUpperCase()}
  }else if(req.params.change == 'lower'){
    returnObject = {word: req.params.word.toLowerCase()}
  }
  res.json(returnObject)
})

app.post('/posts', (req, res, next) => {

  console.log(req.body)
  
  res.json({theReturn: 'nice'})
})

app.listen(port, () => {
let message = `Server started on port: ${port}.
On local network go to: http://localhost:${port}`
  console.log(message)
});