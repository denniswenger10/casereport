const guid = () => {
  const s4 = () => {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

class Case {
  constructor(firstName, lastName, handledAt, category){
    this.id = guid();
    this.user = {firstName, lastName};
    this.user.fullName = this.fullName();
    this.date = new Date;
    this.handledAt = {index: handledAt, text: handledAtArray[handledAt]};
    this.category = {index: category, text: categoryArray[category]};
  }

  fullName(){
    return `${this.user.firstName} ${this.user.lastName}`
  }

}

const handledAtArray = [
  'techbar',
  'office',
  'onsite'
]

const categoryArray = [
  'phone',
  'computer',
  'yadayada',
  'other'
]

let firstCase = new Case('Dennis', 'Wenger' , 2, 0)
let secondCase = new Case('Albin', 'Malmstedt', 0, 1)
let thirdCase = new Case('Adam', 'Syrén', 2, 2)
let fourthCase = new Case('Lars', 'Larsson', 1, 1)

let array = [firstCase, secondCase, thirdCase]

//console.log(JSON.stringify(array))
console.log(JSON.stringify(fourthCase))
